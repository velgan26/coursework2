
package GUI;

import Business_Logic.Garage;
import Business_Logic.Garage;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

/**
 * GUI class
 * @author Fergel
 */
public class GUI extends JFrame{
    
    
    // Creating a Garage object
    Garage mmm = new Garage();
   
    // Creating the frame
    private JFrame myFrame = new JFrame("GARAGE APPLICATION SYSTEM");
    private Container contentPane = myFrame.getContentPane();
    
    // Creating buttons
    private JButton quitButton = new JButton("Quit");
    private JButton addJobButton = new JButton("Add Repair Job");
    private JButton addClearListButton = new JButton("Clear List");
    private JButton addMechanicButton = new JButton("Add Mechanic");
    
    // Creating different JPanels
    private JPanel eastPanel = new JPanel();
    private JPanel westPanel = new JPanel(); 
    private JPanel centerPanel = new JPanel();
    private JPanel northPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    
    JOptionPane show = new JOptionPane();
    
    JFrame frame = new JFrame();
    
    // Creating text areas to show details
    JTextArea text = new JTextArea();
    JTextArea text1 = new JTextArea();
    
    // Creating text field to use it for the repair job
    JTextField field = new JTextField(10);
    JTextField field1 = new JTextField(10);
    JTextField field2 = new JTextField(10);
    JTextField field3 = new JTextField(10);
    JTextField field4 = new JTextField(10);
    
    // Creating check boxes for the repair job
    JCheckBox box1 = new JCheckBox("New Tyres");
    JCheckBox box2 = new JCheckBox("New Exhaust");
    JCheckBox box3 = new JCheckBox("New Type");
    JCheckBox box4 = new JCheckBox("Parts Available");
    
  
    // Constructor to call the necessary methods
    public GUI()
    {
        makeFrame();
        makeMenus(myFrame);
        makeTypes();
        addStaff(); // to add some mechanics to the system 
        addRepairJob(); // to add some repiar jobs to the system
        addPart(); // to add some parts to the system
        assignJob(); // assigning jobs to the system
        reporting(); // generating a report of the completed jobs
    }
     
    
    // Creating the Menu
     private void makeMenus(JFrame frame)
    {
        JMenuBar menubar = new JMenuBar();
        frame.setJMenuBar(menubar);
        
        JMenu fileMenu = new JMenu("FILE");
        menubar.add(fileMenu);
        
        JMenu fileMenu1 = new JMenu("STOCK MANAGER");
        menubar.add(fileMenu1);
        
        JMenu fileMenu4 = new JMenu("MANAGER");
        menubar.add(fileMenu4);
        
        JMenu fileMenu3 = new JMenu("MECHANIC");
        menubar.add(fileMenu3);
        
        JMenu fileMenu2 = new JMenu("HELP");
        menubar.add(fileMenu2);
          
        JMenuItem doneItem = new JMenuItem("Set Job To Complete");
        doneItem.addActionListener(new DoneHandler());
        fileMenu3.add(doneItem);
        
        JMenuItem getAllJobs1 = new JMenuItem("Get All Jobs");
        getAllJobs1.addActionListener(new showGetAllJobs());
        fileMenu3.add(getAllJobs1);
        
        JMenuItem addJob = new JMenuItem("Add New Job");
        addJob.addActionListener(new showAddJobMenu());
        fileMenu3.add(addJob);
        
        JMenuItem jobsPending = new JMenuItem("Jobs Pending");
        jobsPending.addActionListener(new JobsPendingHandler());
        fileMenu3.add(jobsPending);
        
        JMenuItem getAllJobs = new JMenuItem("Get All Jobs");
        getAllJobs.addActionListener(new showGetAllJobs());
        fileMenu4.add(getAllJobs);
        
        JMenuItem getJobsComplete = new JMenuItem("Show Completed Jobs");
        getJobsComplete.addActionListener(new showGetJobsComplete());
        fileMenu4.add(getJobsComplete);
        
        JMenuItem generateReport = new JMenuItem("Reports");
        generateReport.addActionListener(new showGenerateReport());
        fileMenu4.add(generateReport);
        
        JMenuItem assignJob = new JMenuItem("Assign Job");
        assignJob.addActionListener(new showAssignJob());
        fileMenu3.add(assignJob);
        
        JMenu addPart = new JMenu("Add Part");
        addPart.addActionListener(new showAddPart());
        fileMenu1.add(addPart);
        
        JMenuItem getAllParts = new JMenuItem("Show All Parts in Stock");
        getAllParts.addActionListener(new showGetAllParts());
        fileMenu1.add(getAllParts);
        
        JMenuItem getAllParts1 = new JMenuItem("Show All Parts in Stock");
        getAllParts1.addActionListener(new showGetAllParts());
        fileMenu3.add(getAllParts1);
        
        JMenuItem getAllOrderedParts = new JMenuItem("Show All Ordered Parts");
        getAllOrderedParts.addActionListener(new showGetAllOrderedParts());
        fileMenu1.add(getAllOrderedParts);
        
        JMenuItem setPartOutOfStock = new JMenuItem("Set Part Out Of Stock");
        setPartOutOfStock.addActionListener(new showSetPartOutOfStock());
        fileMenu3.add(setPartOutOfStock);
        
        JMenuItem orderPart = new JMenuItem("Order Part");
        orderPart.addActionListener(new showOrderPart());
        fileMenu3.add(orderPart);
        
        JMenuItem addNewPart = new JMenuItem("Add New Part");
        addNewPart.addActionListener(new showAddNewPart());
        addPart.add(addNewPart);
        
        JMenuItem addOrderedPart = new JMenuItem("Add Ordered Part");
        addOrderedPart.addActionListener(new showAddOrderedPart());
        addPart.add(addOrderedPart);
        
        
        JMenu addMechanic = new JMenu("Add Mechanic");
        addMechanic.addActionListener(new showAddMechanicMenu());
        fileMenu4.add(addMechanic);
        
        JMenuItem getAllMechanics = new JMenuItem("Show All Mechanics");
        getAllMechanics.addActionListener(new showGetAllMechanics());
        fileMenu4.add(getAllMechanics);
        
        JMenuItem promoteMechanic = new JMenuItem("Promote Mechanic");
        promoteMechanic.addActionListener(new showPromoteMechanic());
        fileMenu4.add(promoteMechanic);
        
        JMenuItem removeMechanic = new JMenuItem("Remove Mechanic");
        removeMechanic.addActionListener(new showRemoveMechanic());
        fileMenu4.add(removeMechanic);
        
        JMenuItem load = new JMenuItem("Load");
        load.addActionListener(new showLoad());
        fileMenu.add(load);
        
        JMenuItem save = new JMenuItem("Save");
        save.addActionListener(new showSave());
        fileMenu.add(save);
        
        
        JMenuItem trainee = new JMenuItem("TRAINEE");
        trainee.addActionListener(new showAddTrainee());
        addMechanic.add(trainee);
        
        JMenuItem workman = new JMenuItem("WORKMAN");
        workman.addActionListener(new showAddWorkman());
        addMechanic.add(workman);
        
        JMenuItem master = new JMenuItem("MASTER");
        master.addActionListener(new showAddMaster());
        addMechanic.add(master);
        
        JMenuItem about = new JMenuItem("About");
        about.addActionListener(new about());
        fileMenu2.add(about);
        
        JMenuItem version = new JMenuItem("Version");
        version.addActionListener(new version());
        fileMenu2.add(version);
       
        JMenuItem userGuide = new JMenuItem("User Guide");
        userGuide.addActionListener(new userGuide());
        fileMenu2.add(userGuide);
                   
    }

    // Creating the makeFrame method 
    private void makeFrame()
    {    
        
        // main methods in content pane
        contentPane.setLayout(new BorderLayout());
        contentPane.add(eastPanel, BorderLayout.EAST);
        contentPane.add(southPanel, BorderLayout.SOUTH);
        contentPane.add(westPanel, BorderLayout.WEST);
        contentPane.add(centerPanel, BorderLayout.CENTER);
        westPanel.setLayout(new BorderLayout());
        centerPanel.setLayout(new BorderLayout());
        eastPanel.setLayout(new GridLayout(4,1));
        
        
        centerPanel.add(text, BorderLayout.CENTER);
       
        
        eastPanel.add(addJobButton);
        addJobButton.addActionListener(new AddJobHandler());
        
        eastPanel.add(addClearListButton);
        addClearListButton.addActionListener(new clearList());
        
        eastPanel.add(addMechanicButton);
        addMechanicButton.addActionListener(new AddMechanicHandler());
        
        southPanel.add(quitButton);
        quitButton.setVisible(true);
        quitButton.addActionListener(new QuitButtonHandler());
        
        JLabel label = new JLabel("Enter Customer name"); //JLabel to enter the customer name
        JLabel label1 = new JLabel("Insert Car Type"); //JLabel to enter the car type
        JLabel label2 =  new JLabel("Enter Mechanic Name"); //JLabel to enter the Mechanic name
        JLabel lable3 = new JLabel("Enter Mechanic ID number"); //JLabel to enter the Mechanic ID no
        
        JPanel pane = new JPanel();
        pane.setLayout(new GridLayout(2,1));
        pane.add(label);
        pane.add(field);
        westPanel.add(pane, BorderLayout.NORTH);
        
        pane.add(label1);
        pane.add(field1);
        westPanel.add(pane, BorderLayout.NORTH);
        
        
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3,1));
        panel.add(box1);
        panel.add(box2);
        panel.add(box3);
        panel.add(box4);
        westPanel.add(panel, BorderLayout.CENTER);

        
        myFrame.setVisible(true);
        frame.setVisible(false);
        
        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayout(3,1));
        centerPanel.add(panel1, BorderLayout.CENTER);
    }

    // by default the panels are not visible
    private void makeTypes()
    {
       
       westPanel.setVisible(false);
       centerPanel.setVisible(false);
       northPanel.setVisible(false);
       addJobButton.setVisible(false);
       addClearListButton.setVisible(false);
       addMechanicButton.setVisible(false);
       
    }
    
    // Adding mechanics to the system
    private void addStaff()
    {
        mmm.addStaff("A01", "Peter", true);
        mmm.addStaff("A02", "John", true, true);
        mmm.addStaff("A03", "Stephen", true, true, true);
        mmm.addStaff("A04", "Bennet", true, true, true);
        mmm.addStaff("A05", "Hank", true, true, true);
        mmm.addStaff("A06", "Jake", true, true, true);
        mmm.addStaff("A07", "Lauren", true, true, true);
        mmm.addStaff("A08", "Hardy", false, true, true);
        mmm.addStaff("A09", "Josh", true, true, true);
        mmm.addStaff("A10", "Lorry", true);
        mmm.addStaff("A11", "Matt", true);
        mmm.addStaff("A12", "Mel", true, true);
        
    }
    
    // Adding some repair jobs to the system
    private void addRepairJob()
    {
        mmm.addRepairJob("John", "Honda", false, true, true, true);
        mmm.addRepairJob("Jason", "Volvo", true, true, true, true);
        mmm.addRepairJob("Jackson", "Fiat", true, true, false, false);
        mmm.addRepairJob("Johnson", "Audi", false, true, true, true);
        mmm.addRepairJob("Laurel", "Toyota", true, true, true, true);
        mmm.addRepairJob("Samantha", "Rover", false, true, true, false);
        mmm.addRepairJob("Jessica", "Hyundai", false, false, true, true);
        mmm.addRepairJob("Johnny", "Honda", false, true, false, true);
        mmm.addRepairJob("Jack", "Tata", false, true, true, false);
        mmm.addRepairJob("Pete", "Renault", true, true, true, true);
        mmm.addRepairJob("Julio", "Opel", true, true, false, true);
        mmm.addRepairJob("Henry", "Seat", false, true, true, true);
        mmm.addRepairJob("Rene", "Citroen", true, false, false, true);
        mmm.addRepairJob("Rick", "Citroen", false, false, false, true);
        mmm.addRepairJob("Darly", "Porshe", true, false, false, true);
        mmm.addRepairJob("Lucia", "VW", true, false, false, true);
        
    }
    
    // Adding some parts to the system
    private void addPart()
    {
        mmm.addPart("Honda Water Pump", "P01");
        mmm.addPart("Honda LFT Ball Joint", "P02");
        mmm.addPart("Honda RH Ball Joint", "P03");
        mmm.addPart("Fiat Clutch", "P04");
        mmm.addPart("Fiat Pressure Plate", "P05");
        mmm.addPart("Renault FR LFT Lamp", "P06");
        mmm.addPart("Renault FR RH Lamp", "P07");
       
    }
    
    // Assigning some jobs to the system
    private void assignJob()
    {
    
        mmm.assignJob("A06", 6);
        mmm.assignJob("A05", 7);
        mmm.assignJob("A03", 8);
        mmm.assignJob("A04", 4);
        mmm.assignJob("A05", 5);
        mmm.assignJob("A06", 10);
        mmm.assignJob("A07", 11);
        mmm.assignJob("A08", 12);
        mmm.assignJob("A10", 13);
        mmm.assignJob("A11", 14);
        mmm.assignJob("A12", 15);
        
    }
    
    // Generating a report showing the completed jobs
    private void reporting()
    {
        mmm.setJobDone(7);
        mmm.setJobDone(8);
        mmm.setJobDone(4);
        mmm.setJobDone(6);
        mmm.setJobDone(10);
        mmm.setJobDone(11);
        mmm.setJobDone(12);
        mmm.setJobDone(13);
        mmm.setJobDone(5);
        mmm.setJobDone(6);
        mmm.setJobDone(14);
        mmm.setJobDone(15);
    }
     
    // to set the job done
    private class DoneHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        { 
            int jNo;
            
            jNo = Integer.parseInt(JOptionPane.showInputDialog(null, "Insert Job No")); 
            
            JOptionPane.showMessageDialog(myFrame, mmm.setJobDone(jNo));
            
       }
   }
    
   // to display the about section under help 
   private class about implements ActionListener
   {
   
       public void actionPerformed(ActionEvent e)
       {
           
           JOptionPane.showMessageDialog(myFrame, "For any problems please send an email to the email address below: " + "\n" + "Fergeal@gmail.com");
       
       }
   
   } 
   
   // to show the version of the software
   private class version implements ActionListener
   {
   
       public void actionPerformed(ActionEvent e)
       {
           
           JOptionPane.showMessageDialog(myFrame, "Version 1.0");
       
       }
   
   } 
   
   // to show the user guide of the software
   private class userGuide implements ActionListener
   {
   
       public void actionPerformed(ActionEvent e)
       {
           
           JOptionPane.showMessageDialog(myFrame, "                                                    ***** User guide for the Garage Application System *****\n" +
"\n" +
"Different Tabs represent different users, functionalities and permissions.\n" +
"Here below you have what functionalities there are in each tab:\n" +
                 
"\nFILE:\n" +
"\n" +             
"     •	Load – loads data from a repository file – this is currently not set since you will require a repository location from where you run the application\n" +
"     •	Save – saves the information added to the HashMaps to a file\n" +
                   
"\nSTOCK MANAGER:\n" +
"\n" +              
"     •	Add Part:\n" +
"                  i. Add New Part – add a new part to the stock – this requires a part number and the part name/description\n" +
"                 ii. Add Ordered Part – add a part that was ordered – this will only require the part ID no\n" +
"     •	Show All Parts In Stock – to show all the parts that are set “In Stock”\n" +
"     •	Show All Ordered Parts – to show all the parts that are set “Out-Of-Stock”\n" +
                   
"\nMANAGER:\n" +
"\n" +                
"     •	Get All Jobs – to show all the jobs\n" +
"     • Show Completed Jobs – to show all completed jobs\n" +
"     •	Reports – to show all completed jobs in a report style\n" +
"     •	Add Mechanic:\n" +
"                   i. Trainee – to add a trainee mechanic – an ID and a name is required\n" +
"                  ii. Workman – to add a workman mechanic - an ID and a name is required\n" +
"                 iii. Master – to add a master mechanic - an ID and a name is required\n" +
"     •	Show All Mechanics – to show all the mechanics in the system\n" +
"     •	Promote Mechanic – to promote a mechanic to another position – only the ID no is required\n" +
"     •	Remove Mechanic – to remove a mechanic from the system – only the ID no is required\n" +
                   
"\nMECHANIC:\n" +
"\n" +
"     •	Set Jobs To Complete – to set a job to complete – the job no is required\n" +
"     •	Get All Jobs – to see all the jobs added in the system \n" +
"     •	Add New Job – to add a new job to the system – customer name, car model are required\n" +
"     •	Jobs Pending – to check all the pending jobs \n" +
"     •	Assign a Job – to assign a job to a mechanic – job no and mechanic ID are required to assign a job\n" +
"     •	Show All Parts in Stock – to check all the parts that are listed as “In Stock”\n" +
"     •	Set Part Out Of Stock – to set a part out of stock – part ID no is required to set the part out-of-stock\n" +
"     •	Order Part – to order a part – part ID and a part name/description is require – this will automatically add the part to the list but setting it “out-of-stock”\n" +
"\n" +
"");
       
       }
   
   } 
   
   // to generate the report
   private class showGenerateReport implements ActionListener
   {
   
       public void actionPerformed(ActionEvent e)
       {
           JOptionPane.showMessageDialog(myFrame, "                             ***** LIST OF ALL COMPLETED JOBS *****" + "\n" + mmm.getJobsCompleted());
       }
   
   }
   
   // to load 
    private class showLoad implements ActionListener
   {
       public void actionPerformed(ActionEvent e)
       {          
       }
   }
    
   // to save 
   private class showSave implements ActionListener
   {
       public void actionPerformed(ActionEvent e)
       {       
       }
   }
    
   // to show all the jobs
    private class showGetAllJobs implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        {
            
           westPanel.setVisible(false);
           addClearListButton.setVisible(true);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           centerPanel.setVisible(true);
           addMechanicButton.setVisible(false);
           centerPanel.add(text, BorderLayout.EAST);
           text.setVisible(true);
           text1.setVisible(false);
          
           text.setText(mmm.getAllJobs());
        
        }
        
    }
    
    // to show all the ordered parts
    private class showGetAllOrderedParts implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            
           westPanel.setVisible(false);
           addClearListButton.setVisible(true);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           centerPanel.setVisible(true);
           addMechanicButton.setVisible(false);
           centerPanel.add(text, BorderLayout.EAST);
           text.setVisible(true);
           text1.setVisible(false);
          
           text.setText(mmm.getAllOrderedParts());
        
        }
    
    }
    
    // to show all the parts that are in stock
    private class showGetAllParts implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            
           westPanel.setVisible(false);
           addClearListButton.setVisible(true);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           centerPanel.setVisible(true);
           addMechanicButton.setVisible(false);
           centerPanel.add(text, BorderLayout.EAST);
           text.setVisible(true);
           text1.setVisible(false);
          
           text.setText(mmm.getAllParts());
        
        }
    
    }
    
    // to show all the mechanics
    private class showGetAllMechanics implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        {
        
            westPanel.setVisible(false);
           addClearListButton.setVisible(true);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           centerPanel.setVisible(true);
           addMechanicButton.setVisible(false);
           centerPanel.add(text, BorderLayout.EAST);
           text.setVisible(true);
           text1.setVisible(true);
          
           text.setText(mmm.getAllMechanics());
            
        
        }
    
    }
    
    // to show all the completed jobs
    private class showGetJobsComplete implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        {
        
           westPanel.setVisible(false);
           addClearListButton.setVisible(true);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           centerPanel.setVisible(true);
           addMechanicButton.setVisible(false);
           centerPanel.add(text, BorderLayout.EAST);
           text.setVisible(true);
           text1.setVisible(true);
          
           text.setText(mmm.getJobsCompleted());
        
        }
    
    }
    
    
    // to show all the pending jons
    private class JobsPendingHandler implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        {
            
           westPanel.setVisible(false);
           addClearListButton.setVisible(true);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           centerPanel.setVisible(true);
           addMechanicButton.setVisible(false);
           centerPanel.add(text, BorderLayout.EAST);
           text.setVisible(true);
           text1.setVisible(true);
          
           text.setText(mmm.getJobsPending());
           
        }
    
    }
    
   // to add a repair job to the system
    private class showAddJobMenu implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
        
           westPanel.setVisible(true);
           addClearListButton.setVisible(false);
           addJobButton.setVisible(true); //add job button on the east panel will show, to be able to add the job once the details are inserted
           centerPanel.setVisible(false);
           addMechanicButton.setVisible(false);
           field.setVisible(true); 
           field1.setVisible(true);
           text.setVisible(true);
           text1.setVisible(true);
           
        }
    
    }
    
    // to add a part to the system
    private class showAddPart implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
        
           westPanel.setVisible(false);
           addClearListButton.setVisible(false);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           addMechanicButton.setVisible(true);
           centerPanel.setVisible(true);
           field.setVisible(false); 
           field1.setVisible(false);
           field2.setVisible(true);
           field3.setVisible(true);
           field4.setVisible(true);
           text.setVisible(false);
        
        }
    
    
    }
    
    // to add a mechanic to the menu
    private class showAddMechanicMenu implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        {
            
           westPanel.setVisible(false);
           addClearListButton.setVisible(false);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           addMechanicButton.setVisible(true);
           centerPanel.setVisible(true);
           field.setVisible(false); 
           field1.setVisible(false);
           field2.setVisible(true);
           field3.setVisible(true);
           field4.setVisible(true);
           text.setVisible(false);
        
        }
    
    }
    
    // to set the text as visible
    private class AddMechanicHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            text.setVisible(true);
            text.setText("");
        }
    
    }
    
    // to assign a job
    private class showAssignJob implements ActionListener
    {
         public void actionPerformed(ActionEvent e)
        {
            westPanel.setVisible(false);
            
            String idNo;
            int jNo;
            
            idNo = JOptionPane.showInputDialog(null, "Insert Mechanic ID No: "); // id no of the mechanic
            jNo = Integer.parseInt(JOptionPane.showInputDialog(null, "Insert Job No")); // job no
            
            JOptionPane.showMessageDialog(myFrame, mmm.assignJob(idNo, jNo));
        
        }
    
    }
    
    // to remove a mechanic from the system
    private class showRemoveMechanic implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        {
            String idNo;
            
            idNo = JOptionPane.showInputDialog(null, "Insert Mechanic ID No: "); // just the id no of the mechanic is required
            
            int answer = JOptionPane.showConfirmDialog(myFrame,
                "CONFIRM?","Finish", 
                JOptionPane.YES_NO_OPTION); // to confirm removal of mechanic
            
            
            if (answer == JOptionPane.YES_OPTION)
            {
                JOptionPane.showMessageDialog(myFrame, mmm.removeStaff(idNo));
            }              
            
        }
    
    }
    
    // to add a trainee
    private class showAddTrainee implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        { 
             
        String name;
        String id;
        boolean newTyres = true;    
            
        id = JOptionPane.showInputDialog(null, "Please insert ID NO");
        name = JOptionPane.showInputDialog(null, "Please insert Mechanic Name");
        
        
        mmm.addStaff(id, name, newTyres);
        JOptionPane.showMessageDialog(myFrame, "A new Trainee has been added");
        
        }
    
    }
    
    // to promote a mechanic
    private class showPromoteMechanic implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
      
            String idNo;
            
            idNo = JOptionPane.showInputDialog(null, "Please insert ID NO"); // id no is required
            JOptionPane.showMessageDialog(myFrame, mmm.promoteMechanic(idNo));
        
        }
    
    }
    
    // to add a workman
    private class showAddWorkman implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        { 
             
        String name;
        String id;
        boolean newExhaust = true;
        boolean newTypes = true;
            
        id = JOptionPane.showInputDialog(null, "Please insert ID NO");
        name = JOptionPane.showInputDialog(null, "Please insert Mechanic Name");
        
        
        mmm.addStaff(id, name, newExhaust, newTypes);
        JOptionPane.showMessageDialog(myFrame, "A new Workman has been added");
        
        }
    
    }
    
    // to add a master
    private class showAddMaster implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        { 
             
        String name;
        String id;
        boolean newTyres = true;
        boolean newExhaust = true;
        boolean newTypes = true;
            
        id = JOptionPane.showInputDialog(null, "Please insert ID NO");
        name = JOptionPane.showInputDialog(null, "Please insert Mechanic Name");
        
        
        mmm.addStaff(id, name, newTyres, newExhaust, newTypes);
        JOptionPane.showMessageDialog(myFrame, "A new Master has been added");
        
        }
    
    }
    
    // to add a new part 
    private class showAddNewPart implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        {
            String partName;
            String partId;
            
            partName = JOptionPane.showInputDialog(null, "Insert Part Name/Description"); // part name or description
            partId = JOptionPane.showInputDialog(null, "Insert Part Id No");

            mmm.addPart(partName, partId); // no return value
            
            JOptionPane.showMessageDialog(myFrame, "A New Part Has Been Added To The Stock");
        
         }
        
      }
    
    // to order a part
    private class showOrderPart implements ActionListener
    {
        
        public void actionPerformed(ActionEvent e)
        {
        
            String partName;
            String partId;
            
            partName = JOptionPane.showInputDialog(null, "Insert Part Name");
            partId = JOptionPane.showInputDialog(null, "Insert Part Id No");

            
            JOptionPane.showMessageDialog(myFrame, mmm.orderPart(partName, partId));
        
        
        }
    
    
    }
    
    
    // to add the ordered part to the stock
    private class showAddOrderedPart implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String partId;
            
            partId = JOptionPane.showInputDialog(null, "Insert Part Id No");
            
            JOptionPane.showMessageDialog(myFrame, mmm.addOrderedPart(partId));
            
        }
    
    }
    
    // to set a part out-of-stock
    private class showSetPartOutOfStock implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String partId;
            
            partId = JOptionPane.showInputDialog(null, "Insert Part Id No"); // only id is required
            
            JOptionPane.showMessageDialog(myFrame, mmm.setPartOutOfStock(partId));
            
        }
    
    }
        
    
    // to add a repair job to the system
    private class AddJobHandler implements ActionListener
    {
         public void actionPerformed(ActionEvent e)
         {
             
           String cust = "";
           
           boolean avaiParts = false;
           boolean newTyres = false;
           boolean newExhaust = false;
           boolean newTypes = false;
           
           // setting visible panels
           westPanel.setVisible(true);
           addClearListButton.setVisible(false);
           addJobButton.setVisible(true);
           centerPanel.setVisible(false);
           field.setVisible(true); 
           field1.setVisible(true);
           text.setVisible(true);
           
           String name = field.getText();
           String carModel = field1.getText();
           
                 
            if(name.isEmpty() || carModel.isEmpty()){
            JOptionPane.showMessageDialog(myFrame, "CANNOT BE LEFT BLANK"); //this print out is returned
            }else
            {
            if(box1.isSelected()) //if the first check box is checked
            {   //returns
               newTyres = true; 
            }else{
               newTyres = false;
            }
           
            if(box2.isSelected()) //if the second check box is checked
            {    //returns
               newExhaust = true;
            }else{
               newExhaust = false;
            }
           
            if(box3.isSelected()) // if the third check box is checked
            {
             newTypes = true;             
            }else{
             newTypes = false;
            }
            if(box4.isSelected()) // if the fourth check box is checked
            {
             avaiParts = true;
            }else{
             avaiParts = false;
            }
            
           myFrame.setVisible(true); // setting mtFrame visible
          
           String jDetails = mmm.addRepairJob(cust, carModel, avaiParts, newTyres, newExhaust, newTypes); //using the method to add the job
            
           JOptionPane.showMessageDialog(myFrame,jDetails); //showing the job details in an OptionPane message dialog
            }
           
           centerPanel.setVisible(true); 
           
           text.setVisible(true); // setting the textField visible so the details of the job can be displayed
           text.setText("Customer name: " + " " + name + " " + "Parts Replacement " + " "  + newTypes); //showing the details of the job in the text area
           
           
         }
       }
    
    
    /** to clear the text using the clear list button
     * it is using setText to set an empty text area
     */
    private class clearList implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {  
            text.setText(""); //set empty
        }
        
    }

    /** allow quitting the program
     * 
     */
    private class QuitButtonHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        { 
            int answer = JOptionPane.showConfirmDialog(myFrame,
                "Are you sure you want to quit?","Finish", //checks if the user is sure to quit
                JOptionPane.YES_NO_OPTION); //YES_NO Option
            // closes the application
            if (answer == JOptionPane.YES_OPTION)
            {
                System.exit(0); //closes the application
            }              
        }
    }
 
}
