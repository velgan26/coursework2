
package main;

import GUI.GUI;
import Business_Logic.Garage;
import Business_Logic.Parts;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

/**
 * Main class to run the application
 * @author Fergel
 */
public class Main{

    /**
     * Main Class to be able to run the program
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // To call the GUI
        GUI gui = new GUI();
             
    }
    
}
