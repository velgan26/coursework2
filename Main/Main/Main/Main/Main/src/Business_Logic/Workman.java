
package Business_Logic;

/**
 * Child class of parent Employee
 * @author Fergel
 */
public class Workman extends Employee {
    
    public Workman(){
        
        // What necessary is set to true
        setNewExhaust(true);
        setNewTypes(true);
        setNewTyres(false);
    }
    
}
