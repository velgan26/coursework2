
package Business_Logic;

import main.*;

/**
 * The job class to be able to insert all the details of the job
 * @author Fergel
 */
public class Job {
    
    // Different variables required for the job
    private boolean newTyres;
    
    private boolean newTypes;
    
    private boolean newExhaust;
    
    private Customer customer;
    
    private Employee employee;
    
    private String carType;
    
    private static int counter = 0;
    
    private int number;
    
    private String partsAvailability;
    
    public String status;
    
    
   
    // Constructor
    public Job(String carType, boolean newTyres, boolean newTypes, boolean newExhaust){
        
        this.carType = carType;
        this.newExhaust = newExhaust;
        this.newTypes = newTypes;
        this.newTyres = newTyres;
        
    }
    
    // Giving the joba number, always increasing ++
    public void increment()
    {
        counter++;
        number = counter;
    }
    
    public int getJobNo()
    {
        return number;
    }
    
    // To set the status of the job using a switch statement
    public void setStatus(int x)
    {
    
    switch(x)
     {
        case 1: status = "On Going";
        break;
        
        case 0: status = "Working";
        break;
        
        case -1: status = "Pending";
        break;
        
        case -2: status = "Completed";
        break;
        
       default: status = "";
     }  
    
    }
    
    // Setters and Getters
    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }
    
    public Customer getCustomer()
    {
        return customer;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public void setEmployee(Employee employee)
    {
        this.employee = employee;
    }
    
    public Employee getEmployee()
    {
        return employee;
    }
    
    public void setNewTyres(boolean newTyres)
    {
        this.newTyres = newTyres;
    }
    
    public boolean getNewTyres()
    {
        return newTyres;
    }
    
    public void setNewExhaust(boolean newExhaust)
    {
        this.newExhaust = newExhaust;
    }
    
    public boolean getNewExhaust()
    {
        return newExhaust;
    }
    
    public void setNewTypes(boolean newTypes)
    {
        this.newTypes = newTypes;
    }
    
    public boolean getNewTypes()
    {
        return newTypes;
    }
    
    public String getCarType()
    {
        return carType;
    }
    
    public void setPartsAvailability(String partsAvailability)
    {
        this.partsAvailability = partsAvailability;
    }
    
    public String getPartsAvailability()
    {
        return partsAvailability;
    }
    
    // toString method to return all the job details when required
    public String toString()
    {
        return "car type: " + carType + " " + "new tyres: " + newTyres + " " + "new exhaust: " + newExhaust + " " + "new car parts: " + newTypes; 
    }
}
