
package Business_Logic;


/**
 * Interface class
 * @author Fergel
 */
public interface Manager{
    
    /** 
     * To add a Trainee mechanic
     * id and name, newTyres is always true for the trainee mechanic
     * @param id
     * @param name
     * @param newTyres // Trainee can only change new tyres  
     */
    public void addStaff(String id, String name, boolean newTyres);
    
    /**
     * To add a Workman mechanic
     * id and name, newExhaust and newTypes always true for the workman
     * @param id
     * @param name
     * @param newExhaust
     * @param newTypes  // Workman can change new Exhaust and also new types
     */
    public void addStaff(String id, String name, boolean newExhaust, boolean newTypes);
    
    /**
     * To add a Master mechanic
     * id and name, newTyres, newExhaust and newTypes always true for the Master
     * @param id
     * @param name
     * @param newTyres
     * @param newExhaust
     * @param newTypes // Master can do everything, so all variables are true
     */
    public void addStaff(String id, String name, boolean newTyres, boolean newExhaust, boolean newTypes);
    
    
    /**
     * Method to add a job to the garage
     * The method, will add a job to the garage system
     * It will take details about the job and add it to the HashMap
     * It will take the customer name and adds the customer to the ArrayList
     * @param cust
     * @param carModel
     * @param parts
     * @param newTyres
     * @param newExhaust
     * @param newTypes
     * @return 
     */
    
    public String addRepairJob(String cust, String carModel, boolean parts, boolean newTyres, boolean newExhaust, boolean newTypes);
    
    /**
     * Method to assign a job to a mechanic
     * The method, will search for a job and pass it a mechanic according to the job number and the mechanic ID
     * @param idNo
     * @param jNo
     * @return 
     */
    public String assignJob(String idNo, int jNo);
    
    /**
     * Method to add a part to the stock
     * @param partName
     * @param partId 
     */
    public void addPart(String partName, String partId);
    
    /**
     * To return the parts that are ordered "Set out-of-stock"
     * @return 
     */
    public String getAllOrderedParts();
    
    /**
     * Order a part - the part will be added to the HashMap but set to out-of-stock
     * @param partName
     * @param partId
     * @return 
     */
    public String orderPart(String partName, String partId);
    
    /**
     * Method to add the ordered part, the part will be change to "in stock" from out-of-stock
     * id no only will be used to search for the part
     * @param partId 
     * @return 
     */
    public String addOrderedPart(String partId);
    
    /**
     * Method to set the job to completed
     * the job no is required to set the job to completed
     * @param jNo
     * @return 
     */
    public String setJobDone(int jNo);
    
    /**
     * Method to check all the pending jobs
     * this will return a list of pending jobs
     * @return 
     */
    public String getJobsPending();
   
    /**
     * Method to see all the jobs
     * @return 
     */
    public String getAllJobs();
    
    /**
     * Method to be able to see all the jobs that were completed - this will help if reports are required
     * @return 
     */
    public String getJobsCompleted();
    
    /**
     * Method to remove a mechanic from the HashMap
     * only the id is require to remove the staff member
     * @param idNo
     * @return 
     */
    public String removeStaff(String idNo);
    
    /**
     * To show all the mechanics
     * @return 
     */
    public String getAllMechanics();
    
    /**
     * Method to set a part out-of-stock so it can be ordered again
     * only the id no of the part is required to do so
     * @param partId
     * @return 
     */
    public String setPartOutOfStock(String partId);
    
    /**
     * To promote a mechanic from trainee to workman and from workman to master
     * the id no is required to promote the mechanic
     * @param idNo
     * @param name
     * @return 
     */
    public String promoteMechanic(String idNo);
    
    
    
    
    
}
