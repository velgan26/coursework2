
package Business_Logic;

import main.*;

/**
 * Customer class to add the name of the customer when opening a new job
 * @author Fergel
 */
public class Customer {
    
    // Variable for the name
    private String name;
    // Setters and Getters
    // To set the name
    public void setName(String name)
    {
        this.name = name;
    }
   
    // To get the name
    public String getName()
    {
        return name;
    }
    
}
