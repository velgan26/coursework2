
package Business_Logic;

import main.*;
import java.util.*;
import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Garage class
 * this class has all the main methods that are required for this application to work
 * @author Fergel
 */
public class Garage implements Manager{
    
    private String location; // Variable for the garage location
    
    
    // collections
    // to store the jobs
    HashMap <Integer, Job> jobs = new HashMap <Integer, Job>();
    // to store the employees/mechanics
    HashMap <String, Employee> employees = new HashMap <String, Employee>();
    // to store car parts
    HashMap <String, Parts> parts = new HashMap <String, Parts>();
    // to store the customers - customer name cannot be duplicate
    ArrayList <Customer> customers = new ArrayList <Customer>();
    
    // Setter to set the location of the garage
    public void setLocation(String location)
    {
        this.location = location;
    }
    
    // Getter to get the location of the garage
    public String getGarage()
    {
        return location;
    }
    
    // toString to add details to the garage location if required
    public String toString()
    {
        return "Location: " + location;
    }
    
    // Method to add Trainee
    public void addStaff(String id, String name, boolean newTyres){
    
        Trainee trainee = new Trainee();
        trainee.setName(name);
        trainee.setId(id);
        trainee.setStatus(1);
        trainee.setNewTyres(newTyres);
        employees.put(trainee.getId(), trainee); // Adding the mechanic to the HashMap
    
    }
    
    // Method to add a Workman
    public void addStaff(String id, String name, boolean newExhaust, boolean newTypes){
    
        Workman workman = new Workman();
        workman.setName(name);
        workman.setId(id);
        workman.setStatus(1);
        workman.setNewExhaust(newExhaust);
        workman.setNewTypes(newTypes);
        workman.getEmpNo();
        employees.put(workman.getId(), workman); // Adding the mechanic to the HashMap
    
    }
    
    // Method to add a Master
    public void addStaff(String id, String name, boolean newTyres, boolean newExhaust, boolean newTypes){
    
        Master master = new Master();
        master.setName(name);
        master.setId(id);
        master.setStatus(1);
        master.setNewTyres(newTyres);
        master.setNewExhaust(newExhaust);
        master.setNewTypes(newTypes);
        master.getEmpNo();
        employees.put(master.getId(), master); // Adding the mechanic to the HashMap
    
    }
    
    // Method to add a repair job
    public String addRepairJob(String cust, String carModel, boolean parts, boolean newTyres, boolean newExhaust, boolean newTypes){
        
        // To create the customer
        Customer customer = null;
       
        customer = new Customer();
        customer.setName(cust);
        customers.add(customer);
        
        
        // To create the job
        Job j = new Job(carModel, newTyres, newExhaust, newTypes);
        j.increment(); // To give a number to the job
        j.setNewTyres(newTyres);
        j.setNewExhaust(newExhaust);
        j.setNewTypes(newTypes);
        j.setStatus(-1);
        jobs.put(j.getJobNo(), j); // Saving the job to the HashMap
           
        return "Job has been added"; // Returning a message to show that the job has been added
               
   }
    
    // To assign a job to a mechanic 
    public String assignJob(String idNo, int jNo)
    {
        
        try{
        
        Job j = jobs.get(jNo);
        Employee e = employees.get(idNo);
        // Checking if the job is pending and if the Mechanic is available
        if(j != null && j.getStatus().equals("Pending") && e.getStatus().equals("Available"))
        {
            
            j.setEmployee(e);
            j.setStatus(1); // Setting the job to on going
            e.setStatus(0);
            
            return "JOB FOUND and SET to 'ON GOING' - Mechanic ID: " + idNo + " Assigned"; // Return details of the job if the job was assigned
         
        }else
        
        {return "JOB NOT FOUND";} // If the job was not found or is already on going
        }
        catch(NullPointerException npe) // To handle the nullexpection instead of giving an error
        {
           return "MECHANIC ID NOT FOUND"; // If the mechanic is not found
        }  
    }
    
    // To add a part
    public void addPart(String partName, String partId)
    {
        Parts part = new Parts();
        part.Increment();
        part.setStatus(1);
        part.setPartName(partName);
        part.setPartId(partId);
        parts.put(part.getPartId(), part);
    }
    
    // To add a part that was ordered by a mechanic
    public String addOrderedPart(String partId)
    {
         Parts part = parts.get(partId);
         
         if(part != null)
         {
           part.setStatus(1);
           
           return "Part " + partId + " is now 'In Stock'"; // Return part details if the part was ordered and is now in stock
         }
         return "PART NOT FOUND"; // If the part no doesn't exist
    }
    
    
   // A method to order a part 
    public String orderPart(String partName, String partId)
    {
    
        Parts part = new Parts();
        part.Increment();
        part.setStatus(0); // Setting the part out-of-stock
        part.setPartName(partName);
        part.setPartId(partId);
        parts.put(part.getPartId(), part); // Adding the part to the HashMap
        
        return "Part Order Placed"; // Showing that the order was placed
    
    }
    
    // Show all the stock
    public String getAllParts()
    {
    
        String partDetails = ""; //declaring a string for the return value
        
        Collection<Parts> coll = parts.values(); //creating a collection object for the employees HashMap
        
        Iterator iter = coll.iterator(); //using the iterator to be able to loop in the HashMap
        
        while(iter.hasNext())
        {
           Parts part = (Parts) iter.next();  
           
           if(part.getStatus().equals("In Stock"))
           {
             partDetails = partDetails + part.toString() + "\n";
           }
        }
        
        return partDetails; //returning the part details
    }
    
    // To show all the ordered parts
    public String getAllOrderedParts()
    {
    
        String partDetails = ""; //declaring a string for the return value
        
        Collection<Parts> coll = parts.values(); //creating a collection object for the parts HashMap
        
        Iterator iter = coll.iterator(); //using the iterator to be able to loop in the HashMap
        
        while(iter.hasNext())
        {
        
           Parts part = (Parts) iter.next();          
           
           if(part.getStatus().equals("Out Of Stock"))
           {
             partDetails = partDetails + part.toString() + "\n";
           }
           
        
        }
        
        return partDetails; //returning the part details
    }
    
    // To set a part out-of-stock
    public String setPartOutOfStock(String partId)
    {
    
        Parts part = parts.get(partId);
         
         if(part != null)
         {
         
           part.setStatus(0);
           
           return "Part " + partId + " is now 'Out Of Stock'"; // Return part details and showing that the part is not set to out-of-stock
         
         }
    
         return "PART NOT FOUND"; // If the part is not found
    }
    
    // Method to set the job done
    public String setJobDone(int jNo)
    {
    
       Job j = jobs.get(jNo);
       
       if(j != null && j.getStatus().equals("On Going"))
       {
           j.setStatus(-2); // Setting the job to complete
           Employee e = j.getEmployee();
           e.setStatus(1); // Setting the mechanic to available 
           
           return "Job is now complete"; // Return to show the job is not ecomplete
           
       }else
       {return "JOB WAS NOT FOUND";} // If the job is not found
    }
    
    // Showing all the pending jobs
    public String getJobsPending()
    {
        String jobsWaiting = ""; //string to return the jobs waiting
        
        Collection<Job> coll = jobs.values();
        
        Iterator iter = coll.iterator();
             
        while(iter.hasNext())
        {
            Job j = (Job) iter.next();
            
            if(j.getStatus().equals("Pending")) //if the status is equals to waiting
            {
                jobsWaiting = jobsWaiting + "Job No: " + j.getJobNo() + " " + j.toString() + "\n"; //assign the jobDetails 
            }
            
        }
        return jobsWaiting + " "; //return the job details and also an empty space 
    }
    
    // To show all the jobs
    public String getAllJobs()
    {
        String jobDetails = ""; //string to represent the job details
        
        Collection<Job> coll = jobs.values(); //to loop in the collection/HashMap
        
        Iterator iter = coll.iterator();
        
        while(iter.hasNext())
        {
           Job j = (Job) iter.next();          
           jobDetails = jobDetails + "Job No: " + j.getJobNo() + " " + j.toString() + "\n"; //assigning the jobDetails variable to the jobs toString() method
        }
        return jobDetails; //returning the job details
    }
    
    
    // To show all the completed jobs
    public String getJobsCompleted()
    {
        String jobsCompleted = ""; //string to return the jobs waiting
        
        Collection<Job> coll = jobs.values();
        
        Iterator iter = coll.iterator();
             
        while(iter.hasNext())
        {
            Job j = (Job) iter.next();
            
            if(j.getStatus().equals("Completed")) //if the status is equals to completed
            {
                jobsCompleted = jobsCompleted + "Job No: " + j.getJobNo() + " " + j.toString() + "\n"; //assign the jobDetails 
            }
            
        }
        return jobsCompleted + " "; //return the job details and also an empty space 
    }
    
    // Method to remove the staff members
     public String removeStaff(String idNo)
    {    
       Employee employee = employees.get(idNo); //searching the id of the Mechanic
       
        if(employee != null) //if not == to null
       {
           employees.remove(idNo); //remove giving the id number
           
           return "MECHANIC WAS REMOVED"; // If the mechanic was removed
           
       }else{return "MECHANIC NOT FOUND";} // If the mechanic is not found
          
    }
     
     // Showing all mechanics
     public String getAllMechanics()
    {
        String mechanicDetails = ""; //declaring a string for the return value
        
        Collection<Employee> coll = employees.values(); //creating a collection object for the employees HashMap
        
        Iterator iter = coll.iterator(); //using the iterator to be able to loop in the HashMap
        
        while(iter.hasNext())
        {
        
           Employee employee = (Employee) iter.next();          
           mechanicDetails = mechanicDetails + employee.toString(); //setting mechanicDetails to get the details from the toString of the mechanic
        
        }
        return mechanicDetails; //returning the mechanic details
    }
     
     
     // To promote a mechanic
     public String promoteMechanic(String idNo)
    {
        try{
        
        boolean newTyres = false;
        boolean newExhaust = true;
        boolean newTypes = true;
        
        String name = "";
       
        
        Employee employee = employees.get(idNo); // Searching for the id no
        name = employee.getName();
        
        
        
        if(employee != null)
        {
            
            boolean e_nE = employee.getNewExhaust();
            boolean e_nT = employee.getNewTypes();
            boolean e_nTy = employee.getNewTyres();
            
          
            
            if(e_nE == newExhaust && e_nT == newTypes && e_nTy == newTyres) // if the mechanic is a workman
            {
                boolean newTy = true;
                
                Master master = new Master();
                
                master.setId(idNo);
                master.setName(name);
                master.setStatus(1);
                master.setNewTyres(newTy);
                master.setNewExhaust(newExhaust);
                master.setNewTypes(newTypes);
                master.getEmpNo();
                employees.put(master.getId(), master);
                
                return "Mechanic " + idNo +  " promoted to MASTER";
                
            }else if(employee != null && employee.getNewTyres() == true && employee.getNewExhaust() == false) // if the mechanic is a trainee
            {
                 boolean newExh = true;
                 boolean newTy = true;
           
                 Workman workman = new Workman();
            
                 workman.setName(name);
                 workman.setId(idNo);
                 workman.setStatus(1);
                 workman.setNewExhaust(newExh);
                 workman.setNewTypes(newTy);
                 workman.getEmpNo();
                 employees.put(workman.getId(), workman);
        
                 return "Mechanic " + idNo +  " promoted to WORKMAN"; // Promoted to workman
                 
             }else
             {
              return "MECHANIC ALREADY MASTER"; // if the mechani is already a master
             
             }
            
          }
        
        return "NOT FOUND";
        }catch(NullPointerException npe) // If the String reference is not found
        {
            return "MECHANIC NOT FOUND"; 
        }
   }
}    
          
        
          
      



 
    
      

