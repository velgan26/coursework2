
package Business_Logic;

import main.*;

/**
 * Employee class
 * This is a parent class#
 * to add information about the employee
 * @author Fergel
 */
public class Employee {
    
    // Different variables
    private boolean newTyres;
    
    private boolean newExhaust;
    
    private boolean newTypes;
    
    private boolean ready;
    
    private String status;
    
    private String name;
    
    private String id;
    
    private int counter = 0;
    
    private int number;
   
    // Method to change the ststus of the employee using a switch statement
    public void setStatus(int x)
    {
    
     switch(x)
     {
        case 1: status = "Available";
        break;
        
        case 0: status = "On a Job";
        break;
     }  
   }
    
   public String getStatus()
   {
       return status;
   }
    
   // Setters and Getters
   public void setId(String id)
   {
       this.id = id; 
   }
    
   public String getId()
   {
       return id;
   }
   
   public void setName(String name)
   {
       this.name = name;
   }
   
   public String getName()
   {
       return name;
   }
   
   public void setNewTyres(boolean newTyres)
   {
       this.newTyres = newTyres;
   }
   
   public boolean getNewTyres()
   {
       return newTyres;
   }
   
   public void setNewExhaust(boolean newExhaust)
   {
       this.newExhaust = newExhaust;
   }
   
   public boolean getNewExhaust()
   {
       return newExhaust;
   }
   
   public void setNewTypes(boolean newTypes)
   {
       this.newTypes = newTypes;
   }
   
   public boolean getNewTypes()
   {
       return newTypes;
   }
   
   // This is not used 
   public void setReady(boolean ready)
   {
       this.ready = ready;
   }
   
   public boolean getReady()
   {
       return ready;
   }
   
   // To give the employee a number
   public void increment()
    {
        counter++;
        number = counter;
    }
    
   // To get the number of the employee if required
    public int getEmpNo()
    {
        return number;
    }
 
   // toString method to return any required job details
   public String toString()
   {
       return "\n" + "Name: " + name + " id: " + id + " newTyres " + newTyres + " newTypes " + newTypes + " newExhaust " + newExhaust; 
   }
    
    
}
