/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business_Logic;

import Business_Logic.Garage;

import main.*;

/**
 *
 * @author cutaf010
 */
public class Parts {
    
    private String partName;
    
    private String partId;
    
    public String status;
    
    private int counter = 0;
    
    private int number = 0;
    
    
    public void setPartName(String partName){
        this.partName = partName;
    }
    public String getPartName(){
        return partName;
    }
    
    public void setPartId(String partId){
        this.partId = partId;
    }
    public String getPartId(){
        return partId;
    }
    
    public void Increment()
    {
       counter++;
       number = counter;        
    }
    
    public int getIncrement(){
        return number;
    }
    
    public void setStatus(int x)
    {
    
    switch(x)
     {
        case 1: status = "In Stock";
        break;
        
        case 0: status = "Out Of Stock";
        break;
        
       default: status = "";
     }  
    
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public String toString()
    {
        return "Part: " + partName + " Part Id: " + partId + " " + status;
    }
    
}
