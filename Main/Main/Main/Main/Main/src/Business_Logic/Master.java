
package Business_Logic;

/**
 * Child class of parent class Employee
 * @author Fergel
 */
public class Master extends Employee{
    
    
    // everything is require to be true for the master
    public Master()
    {
       setNewExhaust(true);
       setNewTypes(true);
       setNewTyres(true);
        
    }
    
   
}
