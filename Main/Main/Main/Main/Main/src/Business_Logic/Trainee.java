
package Business_Logic;

/**
 * Child class of parent class Employee
 * @author Fergel
 */
public class Trainee extends Employee {
    
    public Trainee(){
    
        // What is require to be true and false for the trainee
        setNewExhaust(false);
        setNewTypes(false);
        setNewTyres(true);
        
    }
    
}
