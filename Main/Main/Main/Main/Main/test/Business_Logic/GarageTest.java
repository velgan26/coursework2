/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business_Logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fergel
 */
public class GarageTest {
    
    public GarageTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setLocation method, of class Garage.
     */
    @Test
    public void testSetLocation() {
        System.out.println("Set Location");
        String location = "Zejtun";
        Garage instance = new Garage();
        
        // Setting the location
        instance.setLocation(location);
    }

    /**
     * Test of getGarage method, of class Garage.
     */
    @Test
    public void testGetGarage() {
        System.out.println("Get Garage");
        Garage instance = new Garage();
        String location = "Zejtun";
        instance.setLocation(location);
        
        // Getting the location
        String result = instance.getGarage();
        assertNotNull(result);
        
    }
    
    

    /**
     * Test of toString method, of class Garage.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Garage instance = new Garage();
     
        
        String result = instance.toString();
        assertNotNull(result);
    }

    /**
     * Test of addStaff method, of class Garage.
     */
    @Test
    public void testAddStaff_3args() {
        System.out.println("Adding a Trainee");
        String id = "A100";
        String name = "Peter";
        boolean newTyres = true;
        Garage instance = new Garage();
        
        // Adding a Trainee
        instance.addStaff(id, name, newTyres); 
        
        // Removing staff that was just added
        String result = instance.removeStaff("A100"); 
        
        String expResult = "MECHANIC WAS REMOVED"; // If mechanic was not correctly added "MECHANIC NOT FOUND" would return
        
        assertEquals(expResult, result); // Result
        
    }

    /**
     * Test of addStaff method, of class Garage.
     */
    @Test
    public void testAddStaff_4args() {
        System.out.println("Adding a Workman");
        String id = "A110";
        String name = "Josh";
        boolean newExhaust = true;
        boolean newTypes = true;
        Garage instance = new Garage();
        
        // Adding a Workman
        instance.addStaff(id, name, newExhaust, newTypes);
        
        // Removing staff that was just added
        String result = instance.removeStaff("A110"); 
        
        String expResult = "MECHANIC WAS REMOVED"; // If mechanic was not correctly added "MECHANIC NOT FOUND" would return
        
        assertEquals(expResult, result); // Result
    }

    /**
     * Test of addStaff method, of class Garage.
     */
    @Test
    public void testAddStaff_5args() {
        System.out.println("Adding a Master");
        String id = "102";
        String name = "Stephen";
        boolean newTyres = true;
        boolean newExhaust = true;
        boolean newTypes = true;
        Garage instance = new Garage();
        
        // Adding a Master
        instance.addStaff(id, name, newTyres, newExhaust, newTypes);  
        
        // Removing staff that was just added
        String result = instance.removeStaff("102"); 
        
        String expResult = "MECHANIC WAS REMOVED"; // If mechanic was not correctly added "MECHANIC NOT FOUND" would return
        
        assertEquals(expResult, result); // Result
    }

    /**
     * Test of addRepairJob method, of class Garage.
     */
    @Test
    public void testAddRepairJob() {
        System.out.println("Add Repair Job");
        String cust = "";
        String carModel = "";
        boolean parts = true;
        boolean newTyres = true;
        boolean newExhaust = true;
        boolean newTypes = true;
        Garage instance = new Garage();
        
        // Result
        String expResult = "Job has been added";
        
        // Adding a repair job
        String result = instance.addRepairJob(cust, carModel, parts, newTyres, newExhaust, newTypes);
        
        assertEquals(expResult, result); // Result
           
    }

    /**
     * Test of assignJob method, of class Garage.
     */
    @Test
    public void testAssignJob() {
        System.out.println("Assign a Job");
        Garage instance = new Garage();
        String idNo1 = "A06";
        String idNo2 = "A07";
        // Creating repair jobs
        instance.addRepairJob("George", "Honda", true, true, true, true);
        instance.addRepairJob("Stephen", "Porsche", true, false, true, true);
        
        // Adding staff members
        instance.addStaff(idNo1, "Peter", true, true, true);
        instance.addStaff(idNo2, "John", true, true, true);
        
        // Checking all the jobs
        System.out.println(instance.getAllJobs());
        int jNo1 = 3;
        int jNo2 = 27;
        
        // Different results
        String expResult1 = "JOB FOUND and SET to 'ON GOING' - Mechanic ID: " + idNo1 + " Assigned";
        String expResult2 = "JOB NOT FOUND";
        String expResult3 = "MECHANIC ID NOT FOUND";
        
        String result1 = instance.assignJob(idNo1, jNo1); // Assigning the job
        String result2 = instance.assignJob(idNo2, jNo2); // Job is not in HashMap
        String result3 = instance.assignJob("A90", 4); // Mechanic is not in HashMap
        
        assertEquals(expResult1, result1); // Different results
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of addPart method, of class Garage.
     */
    @Test
    public void testAddPart() {
        System.out.println("Add Part");
        String partName = "VW pump";
        String partId = "P01";
        Garage instance = new Garage();
        
        // Adding part
        instance.addPart(partName, partId); 
        
        // Setting part out-of-stock
        String result = instance.setPartOutOfStock(partId); // If part was added it will be removed
        
        String expResult = "Part " + partId + " is now 'Out Of Stock'"; // The expected result if the part was correctly added
        
        assertEquals(expResult, result); // Result
    }
    
    /**
     * Test of orderPart method, of class Garage.
     */
    
    @Test
    public void testOrderPart() {
        System.out.println("order Part");
        String partName = "VW Bettle Fuel Pump";
        String partId = "P45";
        
        Garage instance = new Garage();
        
        // Result
        String expResult = "Part Order Placed";
        
        // Ordering part
        String result = instance.orderPart(partName, partId); 
        
        
        assertEquals(expResult, result); // Result
    }

    /**
     * Test of addOrderedPart method, of class Garage.
     */
    @Test
    public void testAddOrderedPart() {
        
        System.out.println("addOrderedPart");
        String partId = "P45";
        Garage instance = new Garage();
        
        // Ordering a part
        instance.orderPart("VW PUMP", "P45");
        
        String result1 = instance.addOrderedPart(partId); // Add the ordered part
        String result2 = instance.addOrderedPart("P50"); // Part not ordered
        
        // Different results
        String expResult1 = "Part " + partId + " is now 'In Stock'";
        String expResult2 = "PART NOT FOUND";
        
        assertEquals(expResult1, result1); // Different results
        assertEquals(expResult2, result2);
        
       
    }


    /**
     * Test of getAllParts method, of class Garage.
     */
    @Test
    public void testGetAllParts() {
        System.out.println("Getting all the parts");
        Garage instance = new Garage();
        String expResult = "Part details: ";
        assertNotNull(expResult + instance.getAllParts() );      
        
    }

    /**
     * Test of getAllOrderedParts method, of class Garage.
     */
    @Test
    public void testGetAllOrderedParts() {
        System.out.println("Show all ordered parts");
        Garage instance = new Garage();
        String partName = "VW PUMP";
        String partId = "P06";
        instance.orderPart(partName, partId);
        String result = instance.getAllOrderedParts();
        assertNotNull(result);
        
    }

    /**
     * Test of setPartOutOfStock method, of class Garage.
     */
    @Test
    public void testSetPartOutOfStock() {
        System.out.println("Set Part Out-Of-Stock");
        
        String partId = "P01";
        String partName = "VW PUMP";
        
        Garage instance = new Garage();
        
        // Adding new part
        instance.addPart(partName, partId);
        
        // Different results
        String expResult1 = "Part " + partId + " is now 'Out Of Stock'"; 
        String expResult2 = "PART NOT FOUND";
        
        String result1 = instance.setPartOutOfStock(partId); // Setting the part out-of-stock
        String result2 = instance.setPartOutOfStock("P02"); // Part not in HashMap
        
        assertEquals(expResult1, result1); // Different results
        assertEquals(expResult2, result2);
        
    }

    /**
     * Test of setJobDone method, of class Garage.
     */
    @Test
    public void testSetJobDone() {
        System.out.println("Set Job Done");
        
        String cust = "Peter";
        String carModel = "Ferrari";
        boolean parts = true;
        boolean newTyres = true;
        boolean newExhaust = true;
        boolean newTypes = true;
        
        Garage instance = new Garage();
        
        // Adding a new repair job
        System.out.println(instance.addRepairJob(cust, carModel, parts, newTyres, newExhaust, newTypes));
        
        // Adding a new staff member
        instance.addStaff("A06", "John", newTyres);
        
        // Assigning the job to the mechanic
        instance.assignJob("A06", 2);
        
        // Different results
        String expResult1 = "Job is now complete";
        String expResult2 = "JOB WAS NOT FOUND";
        String result1 = instance.setJobDone(2); // Finding the job and setting it to complete
        String result2 = instance.setJobDone(7); // Job is not in HashMap
        
        assertEquals(expResult1, result1); // Different results
        assertEquals(expResult2, result2);
       
    }

    /**
     * Test of getJobsPending method, of class Garage.
     */
    @Test
    public void testGetJobsPending() {
        System.out.println("Get Jobs Pending");
        Garage instance = new Garage();
        
        String result = instance.getJobsPending();
     
        assertNotNull(result);
    }

    /**
     * Test of getAllJobs method, of class Garage.
     */
    @Test
    public void testGetAllJobs() {
        System.out.println("Get All Jobs");
        Garage instance = new Garage();
        String result = instance.getJobsPending();
     
        assertNotNull(result);
    }
    

    /**
     * Test of getJobsCompleted method, of class Garage.
     */
    @Test
    public void testGetJobsCompleted() {
        System.out.println("Get Jobs Completed");
        Garage instance = new Garage();
        String result = instance.getJobsCompleted();
        
        assertNotNull(result);

        }

    /**
     * Test of removeStaff method, of class Garage.
     */
    @Test
    public void testRemoveStaff() {
        System.out.println("Remove Staff");
        
        String idNo1 = "A03";
        String idNo2 = "A04";
        Garage instance = new Garage();
        // Adding a staff member
        instance.addStaff(idNo1, "John", true);
        // Creating different results
        String expResult1 = "MECHANIC WAS REMOVED";
        String expResult2 = "MECHANIC NOT FOUND";
        
        // Testing the search
        String result1 = instance.removeStaff(idNo1); // To find
        String result2 = instance.removeStaff(idNo2); // Not to find
        assertEquals(expResult1, result1); // Different results
        assertEquals(expResult2, result2);
    }

    /**
     * Test of getAllMechanics method, of class Garage.
     */
    @Test
    public void testGetAllMechanics() {
        System.out.println("Get All Mechanics");
        Garage instance = new Garage();
       
        String result = instance.getAllMechanics();
        assertNotNull(result);
        System.out.println(result);
        
    }

    /**
     * Test of promoteMechanic method, of class Garage.
     */
    @Test
    public void testPromoteMechanic() {
        System.out.println("Promote Mechanic");
        
        String idNo1 = "A01";
        String idNo2 = "A02";
        String idNo3 = "A03";
        
        Garage instance = new Garage();
        // Creating new staff members
        instance.addStaff(idNo1, "Franky", true);
        instance.addStaff(idNo2, "Jason", false, true, true);
        instance.addStaff(idNo3, "Hank", true ,true, true);
        
        // Creating different results
        String expResult1 = "Mechanic " + idNo1 +  " promoted to WORKMAN";
        String expResult2 = "Mechanic " + idNo2 +  " promoted to MASTER";
        String expResult3 = "MECHANIC ALREADY MASTER";
        String expResult4 = "MECHANIC NOT FOUND";
        
        String result1 = instance.promoteMechanic(idNo1); // Trainee to be promoted to Workman
        String result2 = instance.promoteMechanic(idNo2); // Workman to be promoted MASTER
        String result3 = instance.promoteMechanic(idNo3); // Already Master
        String result4 = instance.promoteMechanic("A04"); // Not in the HashMap
        
        assertEquals(expResult1, result1); // Different results
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        
    }
    
}
