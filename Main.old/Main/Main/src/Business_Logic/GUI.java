/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business_Logic;

import Business_Logic.Garage;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Fergel
 */
public class GUI extends JFrame{
    
    
    
    Garage mmm = new Garage();
   
    private JFrame myFrame = new JFrame("GUI");
    private Container contentPane = myFrame.getContentPane();
    
    private JButton quitButton = new JButton("Quit");
    private JButton addJobButton = new JButton("Add Repair Job");
    private JButton addClearListButton = new JButton("Clear List");
    private JButton addMechanicButton = new JButton("Add Mechanic");
    
    private JPanel eastPanel = new JPanel();
    private JPanel westPanel = new JPanel(); 
    private JPanel centerPanel = new JPanel();
    private JPanel northPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    
    JOptionPane show = new JOptionPane();
    
    JFrame frame = new JFrame();
    
    
    JTextArea text = new JTextArea();
    JTextArea text1 = new JTextArea();
    
    
    JTextField field = new JTextField(10);
    JTextField field1 = new JTextField(10);
    JTextField field2 = new JTextField(10);
    JTextField field3 = new JTextField(10);
    JTextField field4 = new JTextField(10);
    
    JCheckBox box1 = new JCheckBox("New Tyres");
    JCheckBox box2 = new JCheckBox("New Exhaust");
    JCheckBox box3 = new JCheckBox("New Type");
    JCheckBox box4 = new JCheckBox("Parts Available");
    
  
   
    public GUI()
    {
        makeFrame();
        makeMenus(myFrame);
        makeTypes();
    }
     
    
     private void makeMenus(JFrame frame)
    {
        JMenuBar menubar = new JMenuBar();
        frame.setJMenuBar(menubar);
        
        
        
        JMenu fileMenu = new JMenu("MECHANIC GARAGE ENTERPRISE");
        menubar.add(fileMenu);
        
        JMenuItem doneItem = new JMenuItem("Set Job To Complete");
        doneItem.addActionListener(new DoneHandler());
        fileMenu.add(doneItem);
        
        JMenuItem jobsPending = new JMenuItem("Jobs Pending");
        jobsPending.addActionListener(new JobsPendingHandler());
        fileMenu.add(jobsPending);
        
        JMenuItem addJob = new JMenuItem("Add New Job");
        addJob.addActionListener(new showAddJobMenu());
        fileMenu.add(addJob);
        
        JMenu addMechanic = new JMenu("Add Mechanic");
        addMechanic.addActionListener(new showAddMechanicMenu());
        fileMenu.add(addMechanic);
        
        JMenuItem trainee = new JMenuItem("TRAINEE");
        trainee.addActionListener(new showAddTrainee());
        addMechanic.add(trainee);
        
        JMenuItem workman = new JMenuItem("WORKMAN");
        JMenuItem master = new JMenuItem("MASTER");
        
        
        addMechanic.add(workman);
        addMechanic.add(master);
                   
    }

    private void makeFrame()
    {    
        
        // main methods in content pane
        contentPane.setLayout(new BorderLayout());
        contentPane.add(eastPanel, BorderLayout.EAST);
        contentPane.add(southPanel, BorderLayout.SOUTH);
        contentPane.add(westPanel, BorderLayout.WEST);
        contentPane.add(centerPanel, BorderLayout.CENTER);
        westPanel.setLayout(new BorderLayout());
        centerPanel.setLayout(new BorderLayout());
        eastPanel.setLayout(new GridLayout(4,1));
        
       
        
        
        centerPanel.add(text, BorderLayout.CENTER);
       
        
        
        eastPanel.add(addJobButton);
        addJobButton.addActionListener(new AddJobHandler());
        
        eastPanel.add(addClearListButton);
        addClearListButton.addActionListener(new clearList());
        
        eastPanel.add(addMechanicButton);
        addMechanicButton.addActionListener(new AddMechanicHandler());
        
        southPanel.add(quitButton);
        quitButton.setVisible(true);
        quitButton.addActionListener(new QuitButtonHandler());
        
        JLabel label = new JLabel("Enter Customer name"); //JLabel to enter the customer name
        JLabel label1 = new JLabel("Insert Car Type");
        JLabel label2 =  new JLabel("Enter Mechanic Name");
        JLabel lable3 = new JLabel("Enter Mechanic ID number");
        
        JPanel pane = new JPanel();
        pane.setLayout(new GridLayout(2,1));
        pane.add(label);
        pane.add(field);
        westPanel.add(pane, BorderLayout.NORTH);
        
        pane.add(label1);
        pane.add(field1);
        westPanel.add(pane, BorderLayout.NORTH);
        
        
        
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3,1));
        panel.add(box1);
        panel.add(box2);
        panel.add(box3);
        panel.add(box4);
        westPanel.add(panel, BorderLayout.CENTER);

        
        myFrame.setVisible(true);
        
        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayout(3,1));
        centerPanel.add(panel1, BorderLayout.CENTER);
    }

    private void makeTypes()
    {
       
       westPanel.setVisible(false);
       centerPanel.setVisible(false);
       northPanel.setVisible(false);
       addJobButton.setVisible(false);
       addClearListButton.setVisible(false);
       addMechanicButton.setVisible(false);
       
    }
     
    private class DoneHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        { 
            
        }
    }
    
    
    private class JobsPendingHandler implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        {
            
           westPanel.setVisible(false);
           addClearListButton.setVisible(true);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           centerPanel.setVisible(true);
           addMechanicButton.setVisible(false);
           centerPanel.add(text, BorderLayout.CENTER);
           text.setVisible(true);
           
        }
    
    }
    
   
    private class showAddJobMenu implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
        
           westPanel.setVisible(true);
           addClearListButton.setVisible(false);
           addJobButton.setVisible(true); //add job button on the east panel will show, to be able to add the job once the details are inserted
           centerPanel.setVisible(false);
           addMechanicButton.setVisible(false);
           field.setVisible(true); 
           field1.setVisible(true);
        }
    
    }
    
    private class showAddMechanicMenu implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        {
            
            
           westPanel.setVisible(false);
           addClearListButton.setVisible(false);
           addJobButton.setVisible(false); //add job button on the east panel will show, to be able to add the job once the details are inserted
           addMechanicButton.setVisible(true);
           centerPanel.setVisible(true);
           field.setVisible(false); 
           field1.setVisible(false);
           field2.setVisible(true);
           field3.setVisible(true);
           field4.setVisible(true);
           text.setVisible(false);
        
        }
    
    }
    
    private class AddMechanicHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            text.setVisible(true);
            text.setText("hello");
        }
    
    }
    
    private class showAddTrainee implements ActionListener
    {
    
        public void actionPerformed(ActionEvent e)
        { 
             
        String name;
        String id;
        boolean newTyres = true;    
            
        id = JOptionPane.showInputDialog(null, "Please insert ID NO");
        name = JOptionPane.showInputDialog(null, "Please insert Mechanic Name");
        
        
        mmm.addStaff(id, name, newTyres);
        JOptionPane.showMessageDialog(myFrame, "A new Mechanic has been added");
        
        }
    
    }
   
    private class AddJobHandler implements ActionListener
    {
         public void actionPerformed(ActionEvent e)
         {
             
           String cust = "";
           
           boolean avaiParts = false;
           boolean newTyres = false;
           boolean newExhaust = false;
           boolean newTypes = false;
           
           
           westPanel.setVisible(true);
           addClearListButton.setVisible(false);
           addJobButton.setVisible(true);
           centerPanel.setVisible(false);
           field.setVisible(true); 
           field1.setVisible(true);
           
           String name = field.getText();
           String carModel = field1.getText();
           
           boolean trigger = false;
           
           while(trigger == false){
           
            if(name.isEmpty()){
            JOptionPane.showMessageDialog(myFrame, "Customer name cannot be left blank"); //this print out is returned
            }else
            {trigger = true;}
             
            if(carModel.isEmpty()){
            JOptionPane.showMessageDialog(myFrame, "Car Model cannot be left blank");
            }else{trigger = true;}
           
            if(box1.isSelected()) //if the first check box is checked
           {   //returns
               newTyres = true; 
           }else{
               newTyres = false;
           }
           
           if(box2.isSelected()) //if the second check box is checked
           {    //returns
               newExhaust = true;
           }else{
               newExhaust = false;
           }
           
           if(box3.isSelected())
           {
             newTypes = true;             
           }else{
             newTypes = false;
           }
           if(box4.isSelected())
           {
             avaiParts = true;
           }else{
             avaiParts = false;
           }
           
           myFrame.setVisible(true);
          
           String jDetails = mmm.addRepairJob(cust, carModel, avaiParts, newTyres, newExhaust, newTypes); //using the method to add the job
            
           JOptionPane.showMessageDialog(myFrame,jDetails); //showing the job details in an OptionPane message dialog
           
           
           }
           
           centerPanel.setVisible(true); 
           
           
           text.setText("Customer name: " + " " + name + " " + "Parts Replacement " + " "  + newTypes); //showing the details of the job in the text area
           text.setVisible(true);
           
         }
       }
    
    
    /** to clear the text using the clear list button
     * it is using setText to set an empty text area
     */
    private class clearList implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {  
            text.setText(""); //set empty
        }
        
    }

    /** allow quitting the program
     * 
     */
    private class QuitButtonHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        { 
            int answer = JOptionPane.showConfirmDialog(myFrame,
                "Are you sure you want to quit?","Finish", //checks if the user is sure to quit
                JOptionPane.YES_NO_OPTION); //YES_NO Option
            // closes the application
            if (answer == JOptionPane.YES_OPTION)
            {
                System.exit(0); //closes the application
            }              
        }
    }
 
}
