/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business_Logic;

import main.*;

/**
 *
 * @author Fergel
 */
public class Job {
    
    private boolean newTyres;
    
    private boolean newTypes;
    
    private boolean newExhaust;
    
    private Customer customer;
    
    private Employee employee;
    
    private String carType;
    
    private static int counter = 0;
    
    private int number;
    
    private String partsAvailability;
    
    public String status;
    
    
   
    
    public Job(String carType, boolean newTyres, boolean newTypes, boolean newExhaust){
        
        this.carType = carType;
        this.newExhaust = newExhaust;
        this.newTypes = newTypes;
        this.newTyres = newTyres;
        
    }
    
    public void increment()
    {
        counter++;
        number = counter;
    }
    
    public int getJobNo()
    {
        return number;
    }
    
    public void setStatus(int x)
    {
    
    switch(x)
     {
        case 1: status = "On Going";
        break;
        
        case 0: status = "Working";
        break;
        
        case -1: status = "Pending";
        break;
        
        case -2: status = "Completed";
        break;
        
       default: status = "";
     }  
    
    }
    
    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }
    
    public Customer getCustomer()
    {
        return customer;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public void setEmployee(Employee employee)
    {
        this.employee = employee;
    }
    
    public Employee getEmployee()
    {
        return employee;
    }
    
    public boolean getNewTyres()
    {
        return newTyres;
    }
    
    public boolean getNewExhaust()
    {
        return newExhaust;
    }
    
    public boolean getNewTypes()
    {
        return newTypes;
    }
    
    public String getCarType()
    {
        return carType;
    }
    
    public void setPartsAvailability(String partsAvailability)
    {
        this.partsAvailability = partsAvailability;
    }
    
    public String getPartsAvailability()
    {
        return partsAvailability;
    }
    
    public String toString()
    {
        return "car type: " + carType + " " + "new tyres: " + newTyres + " " + "new exhaust: " + newExhaust + " " + "new car parts: " + newTypes; 
    }
}
