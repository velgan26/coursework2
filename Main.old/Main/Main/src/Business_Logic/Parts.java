/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business_Logic;

import Business_Logic.Garage;

import main.*;

/**
 *
 * @author cutaf010
 */
public class Parts {
    
    private String partModel;
    
    private int partId;
    
    private int increment = 0;
    
    public Parts(String partModel, int partId)
    {
        this.partModel = partModel;
        this.partId = partId;
    }
    
    public void setPartModel(String partModel){
        this.partModel = partModel;
    }
    public String getPartModel(){
        return partModel;
    }
    
    public void setPartId(int partId){
        this.partId = partId++;
    }
    public int getPartId(){
        return partId;
    }
    
    public void setIncrement(int increment)
    {
       this.increment = increment++;
    }
    
    public int getIncrement(){
        return increment;
    }
    
}
