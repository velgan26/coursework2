/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business_Logic;


/**
 *
 * @author Fergel
 */
public interface Manager{
    
    public void addStaff(String id, String name, boolean newTyres);
    
    public void addStaff(String id, String name, boolean newExhaust, boolean newTypes);
    
    public void addStaff(String id, String name, boolean newTyres, boolean newExhaust, boolean newTypes);
    
    public String addRepairJob(String cust, String carModel, boolean parts, boolean newTyres, boolean newExhaust, boolean newTypes);
    
    
    
    //public String getJobsPending();
    
    
    
}
