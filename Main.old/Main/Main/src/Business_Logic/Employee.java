/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business_Logic;

import main.*;

/**
 *
 * @author Fergel
 */
public class Employee {
    
    private boolean newTyres;
    
    private boolean newExhaust;
    
    private boolean newTypes;
    
    private boolean ready;
    
    private String status;
    
    private String name;
    
    private String id;
    
    private int counter = 0;
    
    private int number;
   
    public void setStatus(int x)
    {
    
     switch(x)
     {
        case 1: status = "Available";
        break;
        
        case 0: status = "On a Job";
        break;
     }  
   }
    
   public String getStatus()
   {
       return status;
   }
    
   public void setId(String id)
   {
       this.id = id; 
   }
    
   public String getId()
   {
       return id;
   }
   
   public void setName(String name)
   {
       this.name = name;
   }
   
   public String getName()
   {
       return name;
   }
   
   public void setNewTyres(boolean newTyres)
   {
       this.newTyres = newTyres;
   }
   
   public boolean getNewTyres()
   {
       return newTyres;
   }
   
   public void setNewExhaust(boolean newExhaust)
   {
       this.newExhaust = newExhaust;
   }
   
   public boolean getNewExhaust()
   {
       return newExhaust;
   }
   
   public void setNewTypes(boolean newTypes)
   {
       this.newTypes = newTypes;
   }
   
   public boolean getNewTypes()
   {
       return newTypes;
   }
   
   public void setReady(boolean ready)
   {
       this.ready = ready;
   }
   
   public boolean getReady()
   {
       return ready;
   }
   
   public void increment()
    {
        counter++;
        number = counter;
    }
    
    public int getEmpNo()
    {
        return number;
    }
   
   public String toString()
   {
       return "\n" + "Name: " + name + " id: " + id; 
   }
    
    
}
