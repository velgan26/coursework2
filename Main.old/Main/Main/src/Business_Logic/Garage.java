/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business_Logic;

import main.*;
import java.util.*;
import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author Fergel
 */
public class Garage implements Manager{
    
    private String location;
    
    
    // collections
    // to store the jobs
    HashMap <Integer, Job> jobs = new HashMap <Integer, Job>();
    // to store the employees/mechanics
    HashMap <String, Employee> employees = new HashMap <String, Employee>();
    // to store car parts
    HashMap <String, Parts> parts = new HashMap <String, Parts>();
    // to store the customers - customer name cannot be duplicate
    ArrayList <Customer> customers = new ArrayList <Customer>();
    
    
    public void setLocation(String location)
    {
        this.location = location;
    }
    
    public String getGarage()
    {
        return location;
    }
    
    public String toString()
    {
        return "Location: " + location;
    }
    
    
    public void addStaff(String id, String name, boolean newTyres){
    
        Trainee trainee = new Trainee();
        trainee.setName(name);
        trainee.setId(id);
        trainee.setStatus(1);
        trainee.setNewTyres(newTyres);
        employees.put(trainee.getId(), trainee);
    
    }
    
    public void addStaff(String id, String name, boolean newExhaust, boolean newTypes){
    
        Workman workman = new Workman();
        workman.setName(name);
        workman.setId(id);
        workman.setStatus(1);
        workman.setNewExhaust(newExhaust);
        workman.setNewTypes(newTypes);
        workman.getEmpNo();
        employees.put(workman.getId(), workman);
    
    }
    
    public void addStaff(String id, String name, boolean newTyres, boolean newExhaust, boolean newTypes){
    
        Master master = new Master();
        master.setName(name);
        master.setId(id);
        master.setStatus(1);
        master.setNewTyres(newTyres);
        master.setNewExhaust(newExhaust);
        master.setNewTypes(newTypes);
        master.getEmpNo();
        employees.put(master.getId(), master);
    
    }
    
    public String addRepairJob(String cust, String carModel, boolean parts, boolean newTyres, boolean newExhaust, boolean newTypes){
        
        Customer customer = null;
       
        customer = new Customer();
        customer.setName(cust);
        customers.add(customer);
        
        Job j = new Job(carModel, newTyres, newExhaust, newTypes);
        j.increment();
        j.setStatus(-1);
        jobs.put(j.getJobNo(), j);
           
        return "Job has been added and set to pending" +  "\n" + "Job Number: " + j.getJobNo();
            
   }
    
    public String assignJob(String idNo, int jNo)
    {
       
        Job j = jobs.get(jNo);
        Employee e = employees.get(idNo);
        
        if(j != null && j.getStatus().equals("Pending"))
        {
            j.setEmployee(e);
            j.setStatus(1);
            e.setStatus(0);
            
            return "JOB FOUND and SET to ON GOING";
            
        
        }else
        {return "JOB NOT FOUND";
              
    }
  } 
   
}
 
    
      

